Metadata Parser
====================

Summary
---------------------
The metadata parser utilizes a C# wrapper around Pelco's C++ metadata parser component and demonstrates in the simplest terms how to draw boxes around objects found in a scene. 

Features
---------------------
* Draws boxes around objects found in a scene, simply by parsing the video stream.

Building
---------------------
1. git clone https://bitbucket.org/blackey02/metadataparser.git
2. Download and install the Pelco SDK (v.3.5)
3. Open MetadataParser.sln with Visual Studio 2010
4. Done.

Demo Video
---------------------
*Best played back with VLC*

[Demo Video #1](https://copy.com/s7PwNcebSdGT)

Screens
---------------------
![](https://copy.com/pfHTIbDMK1bb)
![](https://copy.com/DIMwpy4SAZEw)

Acknowledgements
---------------------
* [Pelco](http://pdn.pelco.com)