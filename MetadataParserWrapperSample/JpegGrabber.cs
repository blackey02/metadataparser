﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace MetadataParserWrapperSample
{
    class JpegGrabber
    {
        public event Action<Bitmap> GotImage;
        private Uri _uri;
        private bool _stop;

        public int MaxFps { get; set; }

        public JpegGrabber(Uri uri)
        {
            MaxFps = 5;
            _uri = uri;
            ThreadPool.QueueUserWorkItem(obj => GetImages());
        }

        public void Stop()
        {
            _stop = true;
        }

        private void FireGotImage(byte[] buffer)
        {
            if (GotImage != null)
            { 
                using(var ms = new MemoryStream(buffer))
                {
                    var bitmap = new Bitmap(ms);
                    GotImage(bitmap);
                }
            }
        }

        private void GetImages()
        {
            int maxFps = MaxFps;
            int imageInterval = 1000 / maxFps;

            using (WebClient client = new WebClient())
            {
                Stopwatch watch = new Stopwatch();
                while (!_stop)
                {
                    watch.Reset();
                    watch.Start();
                    if (maxFps != MaxFps)
                    {
                        maxFps = MaxFps;
                        imageInterval = 1000 / maxFps;
                    }

                    byte[] buffer = client.DownloadData(_uri);
                    ThreadPool.QueueUserWorkItem(obj => FireGotImage(buffer));
                    watch.Stop();

                    Thread.Sleep((int)Math.Max(0, imageInterval - watch.ElapsedMilliseconds));
                }
            }
        }
    }
}
