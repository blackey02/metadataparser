﻿namespace MetadataParserWrapperSample
{
    partial class MetadataParserWrapperSampleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonCachedSystem = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButtonAddCamera = new System.Windows.Forms.RadioButton();
            this.radioButtonAddSystem = new System.Windows.Forms.RadioButton();
            this.buttonGetCameras = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCachedSystems = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBoxCameras = new System.Windows.Forms.ListBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.pictureBoxMain = new System.Windows.Forms.PictureBox();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBarMain = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).BeginInit();
            this.statusStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonCachedSystem);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxIp);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxPort);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxUsername);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.radioButtonAddCamera);
            this.groupBox1.Controls.Add(this.radioButtonAddSystem);
            this.groupBox1.Controls.Add(this.buttonGetCameras);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxPassword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxCachedSystems);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(167, 406);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setup";
            // 
            // radioButtonCachedSystem
            // 
            this.radioButtonCachedSystem.AutoSize = true;
            this.radioButtonCachedSystem.Checked = true;
            this.radioButtonCachedSystem.Location = new System.Drawing.Point(9, 115);
            this.radioButtonCachedSystem.Name = "radioButtonCachedSystem";
            this.radioButtonCachedSystem.Size = new System.Drawing.Size(99, 17);
            this.radioButtonCachedSystem.TabIndex = 18;
            this.radioButtonCachedSystem.TabStop = true;
            this.radioButtonCachedSystem.Text = "Cached System";
            this.radioButtonCachedSystem.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "IP Address";
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(6, 230);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(152, 20);
            this.textBoxIp.TabIndex = 16;
            this.textBoxIp.Text = "192.168.5.10";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 253);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(6, 269);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(152, 20);
            this.textBoxPort.TabIndex = 14;
            this.textBoxPort.Text = "60001";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Username";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(6, 308);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(152, 20);
            this.textBoxUsername.TabIndex = 12;
            this.textBoxUsername.Text = "admin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 331);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Password";
            // 
            // radioButtonAddCamera
            // 
            this.radioButtonAddCamera.AutoSize = true;
            this.radioButtonAddCamera.Location = new System.Drawing.Point(9, 165);
            this.radioButtonAddCamera.Name = "radioButtonAddCamera";
            this.radioButtonAddCamera.Size = new System.Drawing.Size(83, 17);
            this.radioButtonAddCamera.TabIndex = 7;
            this.radioButtonAddCamera.Text = "Add Camera";
            this.radioButtonAddCamera.UseVisualStyleBackColor = true;
            this.radioButtonAddCamera.CheckedChanged += new System.EventHandler(this.RadioButtonSystem_CheckedChanged);
            // 
            // radioButtonAddSystem
            // 
            this.radioButtonAddSystem.AutoSize = true;
            this.radioButtonAddSystem.Location = new System.Drawing.Point(9, 140);
            this.radioButtonAddSystem.Name = "radioButtonAddSystem";
            this.radioButtonAddSystem.Size = new System.Drawing.Size(81, 17);
            this.radioButtonAddSystem.TabIndex = 6;
            this.radioButtonAddSystem.Text = "Add System";
            this.radioButtonAddSystem.UseVisualStyleBackColor = true;
            this.radioButtonAddSystem.CheckedChanged += new System.EventHandler(this.RadioButtonSystem_CheckedChanged);
            // 
            // buttonGetCameras
            // 
            this.buttonGetCameras.Location = new System.Drawing.Point(6, 377);
            this.buttonGetCameras.Name = "buttonGetCameras";
            this.buttonGetCameras.Size = new System.Drawing.Size(155, 23);
            this.buttonGetCameras.TabIndex = 1;
            this.buttonGetCameras.Text = "Get Cameras";
            this.buttonGetCameras.UseVisualStyleBackColor = true;
            this.buttonGetCameras.Click += new System.EventHandler(this.ButtonGetCameras_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select System";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(6, 347);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(152, 20);
            this.textBoxPassword.TabIndex = 4;
            this.textBoxPassword.Text = "admin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            // 
            // comboBoxCachedSystems
            // 
            this.comboBoxCachedSystems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCachedSystems.FormattingEnabled = true;
            this.comboBoxCachedSystems.Items.AddRange(new object[] {
            "None"});
            this.comboBoxCachedSystems.Location = new System.Drawing.Point(6, 41);
            this.comboBoxCachedSystems.Name = "comboBoxCachedSystems";
            this.comboBoxCachedSystems.Size = new System.Drawing.Size(155, 21);
            this.comboBoxCachedSystems.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBoxCameras);
            this.groupBox2.Controls.Add(this.buttonStart);
            this.groupBox2.Location = new System.Drawing.Point(185, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(167, 406);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Camera";
            // 
            // listBoxCameras
            // 
            this.listBoxCameras.FormattingEnabled = true;
            this.listBoxCameras.Location = new System.Drawing.Point(6, 25);
            this.listBoxCameras.Name = "listBoxCameras";
            this.listBoxCameras.Size = new System.Drawing.Size(155, 342);
            this.listBoxCameras.TabIndex = 3;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(6, 377);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(155, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // pictureBoxMain
            // 
            this.pictureBoxMain.Location = new System.Drawing.Point(358, 13);
            this.pictureBoxMain.Name = "pictureBoxMain";
            this.pictureBoxMain.Size = new System.Drawing.Size(352, 288);
            this.pictureBoxMain.TabIndex = 0;
            this.pictureBoxMain.TabStop = false;
            this.pictureBoxMain.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox1_Paint);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBarMain,
            this.toolStripStatusLabelMain});
            this.statusStripMain.Location = new System.Drawing.Point(0, 445);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(979, 22);
            this.statusStripMain.TabIndex = 3;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripProgressBarMain
            // 
            this.toolStripProgressBarMain.Name = "toolStripProgressBarMain";
            this.toolStripProgressBarMain.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBarMain.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // toolStripStatusLabelMain
            // 
            this.toolStripStatusLabelMain.Name = "toolStripStatusLabelMain";
            this.toolStripStatusLabelMain.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabelMain.Text = "toolStripStatusLabel1";
            // 
            // MetadataParserWrapperSampleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 467);
            this.Controls.Add(this.pictureBoxMain);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MetadataParserWrapperSampleForm";
            this.Text = "Metadata Parser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MetadataParserWrapperSampleForm_FormClosing);
            this.Load += new System.EventHandler(this.MetadataParserWrapperSampleForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).EndInit();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBoxMain;
        private System.Windows.Forms.ComboBox comboBoxCachedSystems;
        private System.Windows.Forms.Button buttonGetCameras;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButtonAddCamera;
        private System.Windows.Forms.RadioButton radioButtonAddSystem;
        private System.Windows.Forms.RadioButton radioButtonCachedSystem;
        private System.Windows.Forms.ListBox listBoxCameras;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMain;
    }
}

