﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataParserWrapperSample.Sprites
{
    class Polygon : Sprite
    {
        public MetaDataParser.Vector[] Vectors { get; set; }

        public Polygon()
        { }
    }
}
