﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataParserWrapperSample.Sprites
{
    class Text : Sprite
    {
        public string Info { get; set; }
        public MetaDataParser.Vector Vector1 { get; set; }

        public Text()
        { }
    }
}
