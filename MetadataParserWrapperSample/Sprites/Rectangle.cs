﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataParserWrapperSample.Sprites
{
    class Rectangle : Sprite
    {
        public MetaDataParser.Vector Vector1 { get; set; }
        public MetaDataParser.Vector Vector2 { get; set; }

        public Rectangle()
        { }
    }
}
