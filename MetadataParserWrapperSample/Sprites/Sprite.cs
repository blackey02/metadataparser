﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MetadataParserWrapperSample.Sprites
{
    abstract class Sprite
    {
        public MetaDataParser.Color BorderColor { get; set; }
        public MetaDataParser.Color FillColor { get; set; }
        public bool Fill { get; set; }

        public Sprite()
        {
            BorderColor = new MetaDataParser.Color() { alpha = 0, blue = 255, green = 0, red = 0 };
            FillColor = new MetaDataParser.Color() { alpha = 0, blue = 255, green = 0, red = 0 };
            Fill = false;
        }
    }
}
