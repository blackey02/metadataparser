﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetadataParserWrapperSample.Sprites;
using System.Threading;

namespace MetadataParserWrapperSample
{
    public partial class MetadataParserWrapperSampleForm : Form, MetaDataParser.IMetaDataRenderer
    {
        private event Action ApplicationLoaded;
        private const string SystemAlias = "MetadataParser";
        private FixedQueue<Sprite> _sprites = new FixedQueue<Sprite>();
        private JpegGrabber _grabber;
        private Pelco.SDK.Stream _stream;

        public MetadataParserWrapperSampleForm()
        {
            InitializeComponent();
            comboBoxCachedSystems.SelectedIndex = 0;
            ApplicationLoaded += new Action(MetadataParserWrapperSampleForm_ApplicationLoaded);
        }

        private void MetadataParserWrapperSampleForm_Load(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(obj => StartApplication());
        }

        private void MetadataParserWrapperSampleForm_ApplicationLoaded()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(MetadataParserWrapperSampleForm_ApplicationLoaded));
                return;
            }

            comboBoxCachedSystems.Items.AddRange(new Pelco.SDK.SystemCollection().ToArray());
        }

        private void FireApplicationLoaded()
        {
            if (ApplicationLoaded != null)
                ApplicationLoaded();
        }

        private void StartApplication()
        {
            StartProgress("Starting Application");
            Pelco.SDK.Application.Startup();
            var systems = new Pelco.SDK.SystemCollection();
            bool hasEdgeSystem = systems.Any(system => system.SystemAlias == SystemAlias);
            if (!hasEdgeSystem)
                systems.Add(string.Format("pelcoedgedevices://?alias={0}", SystemAlias));
            FireApplicationLoaded();
            EndProgress();
        }

        private void ShutdownApplication()
        {
            StartProgress("Stopping Application");
            Application.DoEvents();
            Pelco.SDK.Application.Shutdown();
            if (_grabber != null)
            {
                _grabber.GotImage -= new Action<Bitmap>(Grabber_GotImage);
                _grabber.Stop();
                _grabber = null;
            }
            if (_stream != null)
            {
                _stream.EncodedMediaFrames -= new Pelco.SDK.Stream.MediaFrameDelegate(Stream_EncodedMediaFrames);
                _stream.Stop();
                _stream.Dispose();
                _stream = null;
            }
            EndProgress();
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            int x, y, w, h;

            Action<MetaDataParser.Color, Action<Brush>> GetBrushAndInvoke = (color, method) =>
                {
                    var fillColor = Color.FromArgb((int)color.alpha, (int)color.red, (int)color.green, (int)color.blue);
                    using(var brush = new SolidBrush(fillColor))
                    {
                        method(brush);
                    }
                };

            foreach (var sprite in _sprites.Items)
            {
                if (sprite is Line)
                {
                    var line = sprite as Line;
                    var points = new PointF[2];
                    points[0].X = line.Vector1.x;
                    points[0].Y = line.Vector1.y;
                    points[1].X = line.Vector2.x;
                    points[1].Y = line.Vector2.y;

                    GetBrushAndInvoke(line.BorderColor,
                            new Action<Brush>(brush => e.Graphics.DrawLine(new Pen(brush), points[0], points[1])));
                }
                else if (sprite is Polygon)
                {
                    var polygon = sprite as Polygon;
                    var points = new PointF[polygon.Vectors.Length];

                    for(int i = 0; i < points.Length; i++)
                    {
                        var point = points[i];
                        var vector = polygon.Vectors[i];
                        point.X = vector.x;
                        point.Y = vector.y;
                    }

                    if (polygon.Fill)
                    {
                        GetBrushAndInvoke(polygon.FillColor,
                            new Action<Brush>(brush => e.Graphics.FillPolygon(brush, points)));
                    }
                    else
                    {
                        GetBrushAndInvoke(polygon.BorderColor,
                            new Action<Brush>(brush => e.Graphics.DrawPolygon(new Pen(brush), points)));
                    }
                }
                else if (sprite is Sprites.Rectangle)
                {
                    var rect = sprite as Sprites.Rectangle;
                    x = (int)rect.Vector1.x;
                    y = (int)rect.Vector1.y;
                    w = (int)rect.Vector2.x - x;
                    h = (int)rect.Vector2.y - y;

                    if(rect.Fill)
                    {
                        GetBrushAndInvoke(rect.FillColor, 
                            new Action<Brush>(brush => e.Graphics.FillRectangle(brush, new System.Drawing.Rectangle(x, y, w, h))));
                    }
                    else
                    {
                        GetBrushAndInvoke(rect.BorderColor,
                            new Action<Brush>(brush => e.Graphics.DrawRectangle(new Pen(brush), new System.Drawing.Rectangle(x, y, w, h))));
                    }
                }
                else if (sprite is Text)
                {
                    var txt = sprite as Text;
                    var text = txt.Info;
                    var font = new Font("Tahoma", 14);
                    var point = new PointF(txt.Vector1.x, txt.Vector1.y);

                    GetBrushAndInvoke(txt.BorderColor,
                            new Action<Brush>(brush => e.Graphics.DrawString(text, font, brush, point)));
                }
            }
        }

        #region IMetaDataRenderer Members
        public void BeginDraw()
        {}

        public void DrawLine(MetaDataParser.Vector v1, MetaDataParser.Vector v2, MetaDataParser.Color color)
        {
            var line = new Line();
            line.Vector1 = v1;
            line.Vector2 = v2;
            line.BorderColor = color;
            _sprites.Enqueue(line);
        }

        public void DrawPolygon(MetaDataParser.Vector[] vectors, MetaDataParser.Color fillColor, MetaDataParser.Color borderColor, bool fill)
        {
            var polygon = new Polygon();
            polygon.Vectors = vectors;
            polygon.FillColor = fillColor;
            polygon.BorderColor = borderColor;
            polygon.Fill = fill;
            _sprites.Enqueue(polygon);
        }

        public void DrawRectangle(MetaDataParser.Vector v1, MetaDataParser.Vector v2, MetaDataParser.Color color, bool fill)
        {
            var rect = new Sprites.Rectangle();
            rect.Vector1 = v1;
            rect.Vector2 = v2;
            rect.BorderColor = color;
            rect.FillColor = color;
            rect.Fill = fill;
            _sprites.Enqueue(rect);
        }

        public void DrawText(string text, MetaDataParser.Vector v1, MetaDataParser.Color color)
        {
            var txt = new Text();
            txt.Info = text;
            txt.Vector1 = v1;
            txt.BorderColor = color;
            _sprites.Enqueue(txt);
        }

        public void EndDraw()
        {}
        #endregion

        private void StartProgress(string info)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<string>(StartProgress), new object[] { info });
                return;
            }

            toolStripStatusLabelMain.Text = info;
            toolStripStatusLabelMain.Visible = true;
            toolStripProgressBarMain.Visible = true;
        }

        private void EndProgress()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(EndProgress));
                return;
            }

            toolStripStatusLabelMain.Visible = false;
            toolStripProgressBarMain.Visible = false;
            toolStripStatusLabelMain.Text = string.Empty;
        }

        private void RadioButtonSystem_CheckedChanged(object sender, EventArgs e)
        {
            var radio = sender as RadioButton;
            if (radio.Checked)
            {
                if (radio == radioButtonAddSystem)
                    textBoxPort.Text = "60001";
                else if (radio == radioButtonAddCamera)
                    textBoxPort.Text = "80";
            }
        }

        private void MetadataParserWrapperSampleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ShutdownApplication();
        }

        private bool AddSystem()
        {
            bool success = true;
            try
            {
                var systems = new Pelco.SDK.SystemCollection();
                var scheme = string.Format("{0}:{1}@pelcosystem://{2}:{3}", textBoxUsername.Text, textBoxPassword.Text, textBoxIp.Text, textBoxPort.Text);
                var system = new Pelco.SDK.System(scheme);
                Invoke(new Action(() =>
                    {
                        comboBoxCachedSystems.Items.Add(system);
                        comboBoxCachedSystems.SelectedItem = system;
                    }));
            }
            catch (Pelco.SDK.Exception e)
            {
                success = false;
                Console.WriteLine(string.Format("Error: {0}", e.Message));
            }
            return success;
        }

        private bool AddCamera()
        {
            bool success = true;
            try
            {
                var system = new Pelco.SDK.SystemCollection().GetItemByKey(SystemAlias);
                var scheme = string.Format("{0}:{1}@pelcoedgedevices://{2}:{3}", textBoxUsername.Text, textBoxPassword.Text, textBoxIp.Text, textBoxPort.Text);
                system.DeviceCollection.Add(scheme);
                Invoke(new Action(() => comboBoxCachedSystems.SelectedItem = system));
            }
            catch (Pelco.SDK.Exception e)
            {
                success = false;
                Console.WriteLine(string.Format("Error: {0}", e.Message));
            }
            return success;
        }

        private void LoadCameras()
        {
            var system = comboBoxCachedSystems.SelectedItem as Pelco.SDK.System;
            if (system != null)
            {
                system.Login(textBoxUsername.Text, textBoxPassword.Text);
                try
                {
                    var cameras = system.DeviceCollection.Where(sys => sys.DeviceType == Pelco.SDK.DeviceType.Camera);
                    listBoxCameras.Items.AddRange(cameras.ToArray());
                }
                catch (Pelco.SDK.Exception e)
                {
                    Console.WriteLine(string.Format("Error: {0}", e.Message));
                }
            }
        }

        private void ButtonGetCameras_Click(object sender, EventArgs e)
        {
            listBoxCameras.Items.Clear();
            ThreadPool.QueueUserWorkItem(obj => ButtonGetCameras());
        }

        private void ButtonGetCameras()
        {
            StartProgress("Loading Cameras");
            if (radioButtonAddSystem.Checked)
                AddSystem();
            else if (radioButtonAddCamera.Checked)
                AddCamera();
            BeginInvoke(new Action(LoadCameras));
            EndProgress();
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            if (_grabber != null)
            {
                _grabber.GotImage -= new Action<Bitmap>(Grabber_GotImage);
                _grabber.Stop();
                _grabber = null;
            }

            if (_stream != null)
            {
                _stream.EncodedMediaFrames -= new Pelco.SDK.Stream.MediaFrameDelegate(Stream_EncodedMediaFrames);
                _stream.Stop();
                _stream.Dispose();
                _stream = null;
            }

            var device = listBoxCameras.SelectedItem as Pelco.SDK.Device;
            if (device != null)
            {
                var camera = new Pelco.SDK.Camera(device);
                _stream = camera.CreateStream();
                _stream.EncodedMediaFrames += new Pelco.SDK.Stream.MediaFrameDelegate(Stream_EncodedMediaFrames);
                _stream.Play(1);

                string uri = string.Format("http://{0}:{1}/jpeg", camera.IP, camera.Port);
                _grabber = new JpegGrabber(new Uri(uri));
                _grabber.GotImage += new Action<Bitmap>(Grabber_GotImage);
            }
        }

        private void Stream_EncodedMediaFrames(Pelco.SDK.MediaFrame frame)
        {
            using (MetaDataParser.Wrapper wrapper = new MetaDataParser.Wrapper())
            {
                wrapper.SetData(frame.Frame);

                bool hasTimestamp = wrapper.HasTimestamp();
                if (hasTimestamp)
                {
                    string ts = wrapper.GetTimestampAsString(false);
                    DateTime dt = new DateTime();
                    wrapper.GetTimestamp(ref dt);
                    //Console.WriteLine(string.Format("{0} // {1}", ts, dt));
                }

                bool hasDrawingData = wrapper.HasDrawingData();
                if (hasDrawingData)
                {
                    using (MetaDataParser.DrawingData drawingData = wrapper.GetDrawingData())
                    {
                        var nativeRenderer = new MetaDataParser.MetaDataRenderer(this);
                        nativeRenderer.SetDrawingData(drawingData);
                        nativeRenderer.RenderDrawingData(pictureBoxMain.Width, pictureBoxMain.Height);
                        drawingData.Clear();
                    }
                }
                wrapper.Clear();
            }
        }

        private void Grabber_GotImage(Bitmap obj)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<Bitmap>(Grabber_GotImage), new object[] { obj });
                return;
            }

            if (pictureBoxMain.Width != obj.Width || pictureBoxMain.Height != obj.Height)
            {
                pictureBoxMain.Width = obj.Width;
                pictureBoxMain.Height = obj.Height;
            }

            pictureBoxMain.Image = obj;
        }
    }
}
