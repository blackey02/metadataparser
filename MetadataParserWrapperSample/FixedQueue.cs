﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace MetadataParserWrapperSample
{
    class FixedQueue<T>
    {
        private ConcurrentQueue<T> _queue = new ConcurrentQueue<T>();
        public int Limit { get; set; }

        public FixedQueue()
        {
            Limit = 100;
        }

        public ConcurrentQueue<T> Items
        {
            get
            {
                ConcurrentQueue<T> items;
                lock (this)
                {
                    items = new ConcurrentQueue<T>(_queue);
                }
                return items;
            }
        }

        public void Enqueue(T item)
        {
            lock (this)
            {
                _queue.Enqueue(item);
                T overflow;
                while (_queue.Count > Limit && _queue.TryDequeue(out overflow));
            }
        }
    }
}
