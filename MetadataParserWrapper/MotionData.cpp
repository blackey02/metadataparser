#include "stdafx.h"
#include "MotionData.h"

using namespace MetaDataParser;

MotionData::MotionData() {
	_impl = new PelcoAPI::MotionData();
	_rawBuffer = NULL;
}

MotionData::!MotionData() {
	delete _impl;
	if(_rawBuffer)
		delete [] _rawBuffer;
}

MotionData::~MotionData() {
	this->!MotionData();
}

bool MotionData::SetData(unsigned char *buffer, unsigned int length) {
	if(_rawBuffer)
		delete [] _rawBuffer;

	_rawBufferLen = length;
	_rawBuffer = new unsigned char[_rawBufferLen];
	std::memcpy(_rawBuffer, buffer, _rawBufferLen);
	_impl->SetData(_rawBuffer, _rawBufferLen);
	return true;
}

unsigned int MotionData::Rows() {
	return _impl->Rows();
}

unsigned int MotionData::Columns() {
	return _impl->Columns();
}

array<System::Byte>^ MotionData::Bitmask() {
	unsigned int length = _impl->BitmaskLength();
	array<System::Byte> ^bitMask = gcnew array<System::Byte>(length);
	pin_ptr<System::Byte> p = &bitMask[bitMask->GetLowerBound(0)];
	std::memcpy(p, _impl->Bitmask(), length);
	return bitMask;
}

void MotionData::Clear() {
	_impl->Clear();
}

unsigned char* MotionData::RawBuffer() {
	return _rawBuffer;
}

int MotionData::RawBufferLen() {
	return _rawBufferLen;
}