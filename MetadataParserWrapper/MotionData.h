#pragma once

#include "PelcoAPI\MotionData.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {	
	public ref class MotionData
	{ 
	protected:
		PelcoAPI::MotionData *_impl;
		unsigned char *_rawBuffer;
		int _rawBufferLen;

	internal:
		bool SetData(unsigned char *buffer, unsigned int length);
		unsigned char* RawBuffer();
		int RawBufferLen();
	
	public:
		MotionData();
		!MotionData();
		virtual ~MotionData();

		unsigned int Rows();
		unsigned int Columns();
		array<System::Byte>^ Bitmask();
		void Clear();
	};
}