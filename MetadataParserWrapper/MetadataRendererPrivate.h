#pragma once

#include <vcclr.h>
#include "PelcoAPI/MetaDataRenderer.h"
#include "IMetaDataRenderer.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {
	class MetaDataRendererPrivate : public PelcoAPI::MetaDataRenderer
	{
	public:
		MetaDataRendererPrivate(IMetaDataRenderer ^renderer);
		virtual ~MetaDataRendererPrivate();

	protected:
		gcroot<IMetaDataRenderer^> _renderer;

	protected:
		//=========================================================================
		// OPTIONAL base class overrides.
		//=========================================================================
		virtual void BeginDraw() throw();
		virtual void EndDraw() throw();
		virtual PelcoAPI::VECTOR TransformVectorForDisplay(const PelcoAPI::VECTOR &v) throw();

		//=========================================================================
		// REQUIRED base class overrides.
		//=========================================================================
		virtual void DrawLine(const PelcoAPI::VECTOR &v1, const PelcoAPI::VECTOR &v2, PelcoAPI::COLOR color) throw();
		virtual void DrawRectangle(const PelcoAPI::VECTOR &v1, const PelcoAPI::VECTOR &v2, PelcoAPI::COLOR color, bool fill) throw();
		virtual void DrawPolygon(PelcoAPI::VECTOR *vectors, unsigned int count,PelcoAPI::COLOR fillColor, PelcoAPI::COLOR borderColor, bool fill) throw();
		virtual void DrawText(const std::string &text, const PelcoAPI::VECTOR &location, PelcoAPI::COLOR color) throw();
	};
}