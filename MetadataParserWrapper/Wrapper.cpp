#include "stdafx.h"
#include "Wrapper.h"

using namespace MetaDataParser;

Wrapper::Wrapper() {
	_impl = new PelcoAPI::MetaDataParser();
}

Wrapper::!Wrapper() {
	delete _impl;
}

Wrapper::~Wrapper() {
	this->!Wrapper();
}

void Wrapper::SetData(array<System::Byte> ^buffer) {
	pin_ptr<System::Byte> p = &buffer[buffer->GetLowerBound(0)];
	_impl->SetData(p, (unsigned)buffer->Length);
}

bool Wrapper::HasTimestamp() {
	return _impl->HasTimestamp();
}

System::String^ Wrapper::GetTimestampAsString(bool localtime) {
	const char *timestamp = _impl->GetTimestampAsString(localtime);
	System::String ^timestampStr = gcnew System::String(timestamp);
	return timestampStr;
}

bool Wrapper::GetTimestamp(System::DateTime %dateTime) {
	struct timeval *timeVal = new struct timeval();
	struct tm *tmVal = new struct tm();
	unsigned int dstOffsetVal = 0;
	unsigned int timeZoneVal = 0;
	
	_impl->GetTimestamp(false, timeVal, tmVal, &dstOffsetVal, &timeZoneVal);

	dateTime = System::DateTime(tmVal->tm_year+1900, tmVal->tm_mon+1, tmVal->tm_mday, tmVal->tm_hour, tmVal->tm_min, 
		tmVal->tm_sec, timeVal->tv_usec / 1000, System::DateTimeKind::Utc);
	
	delete timeVal;
	delete tmVal;
	return true;
}

bool Wrapper::HasMotionData() {
	return _impl->HasMotionData();
}

MotionData ^Wrapper::GetMotionData() {
	MotionData ^motionData = nullptr;
	
	if(this->HasMotionData()) {
		int length = 0;
		_impl->GetMotionDataRaw(NULL, &length);

		if(length > 0) {
			unsigned char *buffer = new unsigned char[length];
			_impl->GetMotionDataRaw(buffer, &length);
			motionData = gcnew MotionData();
			motionData->SetData(buffer, length);
			delete buffer;
		}
	}
	return motionData;
}

bool Wrapper::HasDrawingData() {
	return _impl->HasDrawingData();
}

DrawingData^ Wrapper::GetDrawingData() {
	DrawingData ^drawingData = nullptr;

	if(this->HasDrawingData()) {
		int length = 0;
		_impl->GetDrawingDataRaw(NULL, &length);

		if(length > 0) {
			unsigned char *buffer = new unsigned char[length];
			_impl->GetDrawingDataRaw(buffer, &length);
			drawingData = gcnew DrawingData();
			drawingData->SetData(buffer, length);
			delete buffer;
		}
	}
	return drawingData;
}

void Wrapper::Clear() {
	_impl->Clear();
}