#pragma once

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {
	public value struct Color
	{
		/// The alpha transparency value.
		unsigned int alpha;
		/// The red level.
		unsigned int red;
		/// The green level.
		unsigned int green;
		/// The blue level.
		unsigned int blue;
	};

	public value struct Vector
	{
		/// The length of the vector in the X plane
		float x;
		/// The length of the vector in the Y plane
		float y;
		/// The length of the vector on the Z plane.
		float z;
	};

	public value struct Point {
		long x;
		long y;
	};

	public enum class Direction {
		NORTH=0,
		NORTH_EAST,
		EAST,
		SOUTH_EAST,
		SOUTH,
		SOUTH_WEST,
		WEST,
		NORTH_WEST,
		ANY
	};

	public enum class ObjectType {
		NO_OBJECT_TYPE = 0,
		PERSON,
		GROUP_OF_PEOPLE,
		VEHICLE,
		/// This system reserved value that needs to be last. It is used for validity testing.
		OBJECT_TYPES_COUNT
	};

	public enum class PrimitiveColor {
		RED=0,
		BLUE,
		LIME,
		FUCHSIA,
		YELLOW,
		AQUA,
		GREEN,
		MAROON,
		NAVY,
		OLIVE,
		PURPLE,
		GRAY,
		TEAL,
		BLACK,
		WHITE,
		SILVER
	};

	public enum class PrimitiveType {
		/// Not a primitive
		NONE = 0,
		/// A primitive that encompasses two points on a 2D plane.
		LINE,
		/// A primitive that encompasses three or more points on a 2D plane.	
		POLYGON,
		/// A polygon with an inner area filled with a color different from the background.
		POLYGON_FILLED,
		/// A smooth closed curve which is symmetric about its X and Y axes.
		ELLIPSE,
		/// An elipse with an inner area filled with a color different from the background.
		ELLIPSE_FILLED,
		/// A combination of ASCII characters.
		TEXT,
		/// A control primitive, the square that tracks the position on the scroll bar
		TRACK,
		/// A line with a triangle at the tip, denoting the pointed direction.
		ARROW,
		/// A control primitive, the line on the scroll bar
		TRACK_LINE,
		/// This system reserved value that needs to be last. It is used for validity testing.
		TYPES_COUNT	
	};
}