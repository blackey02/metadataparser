#pragma once

#include "PelcoAPI\MetaDataParser.h"
#include "MotionData.h"
#include "DrawingData.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {	
	public ref class Wrapper
	{ 
	protected:
		PelcoAPI::MetaDataParser *_impl;

	public:
		Wrapper();
		!Wrapper();
		virtual ~Wrapper();

		void SetData(array<System::Byte> ^buffer);
		bool HasTimestamp();
		System::String^ GetTimestampAsString(bool localtime);
		bool GetTimestamp(System::DateTime %dateTime);
		bool HasMotionData();
		MotionData^ GetMotionData();
		bool HasDrawingData();
		DrawingData^ GetDrawingData();
		void Clear();
	};
}
