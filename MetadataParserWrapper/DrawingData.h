#pragma once

#include "PelcoAPI/DrawingData.h"
#include "DrawingPrimitive.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {	
	public ref class DrawingData
	{ 
	protected:
		PelcoAPI::DrawingData *_impl;
		unsigned char *_rawBuffer;
		int _rawBufferLen;
	
	internal:
		bool SetData(unsigned char *buffer, unsigned int length);
		unsigned char* RawBuffer();
		int RawBufferLen();

	public:
		DrawingData();
		!DrawingData();
		virtual ~DrawingData();

		void ResetPosition();
		DrawingPrimitive^ GetNextPrimitive();
		void Clear();
	};
}