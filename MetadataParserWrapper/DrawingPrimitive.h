#pragma once

#include "PelcoAPI\DrawingPrimitive.h"
#include "CommonDefs.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {
	public ref class DrawingPrimitive
	{
	protected:
		PelcoAPI::DrawingPrimitive *_impl;

	internal:
		DrawingPrimitive(PelcoAPI::DrawingPrimitive *primitive);

	public:
		!DrawingPrimitive();
		virtual ~DrawingPrimitive();
		
		PrimitiveType GetPrimitiveType();
		unsigned int GetNumberOfPoints();
		Point GetPoint(unsigned int index);
		PrimitiveColor GetDrawColor();
		System::String^ GetText();
		PrimitiveColor GetFillColor();
		unsigned char GetObjectId();
		ObjectType GetObjectType();
		Direction GetDirection();
		bool GetArrowHead(Point %point1, Point %point2, Point %point3);
		bool GetArrowTail(Point %point1, Point %point2);
	};
}