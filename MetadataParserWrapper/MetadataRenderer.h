#pragma once

#include "MetaDataRendererPrivate.h"
#include "IMetaDataRenderer.h"
#include "MotionData.h"
#include "DrawingData.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {
	public ref class MetaDataRenderer
	{
	protected:
		MetaDataRendererPrivate *_impl;

	public:
		MetaDataRenderer(IMetaDataRenderer ^renderer);
		!MetaDataRenderer();
		virtual ~MetaDataRenderer();

	public:
		void SetMotionData(array<System::Byte> ^buffer, Color color);
		void SetMotionData(MotionData ^data, Color color);
		void SetDrawingData(array<System::Byte> ^buffer);
		void SetDrawingData(DrawingData ^data);
		void RenderMotionData();
		void RenderDrawingData(int width, int height);
		void Reset();
	};
}