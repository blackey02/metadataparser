#pragma once

#include "CommonDefs.h"

using namespace System::Runtime::InteropServices;

namespace MetaDataParser {
	public interface class IMetaDataRenderer {
		void BeginDraw();
		void EndDraw();
		void DrawLine(const Vector v1, const Vector v2, Color color);
		void DrawRectangle(const Vector v1, const Vector v2, Color color, bool fill);
		void DrawPolygon(array<Vector> ^vectors, Color fillColor, Color borderColor, bool fill);
		void DrawText(const System::String ^text, const Vector v1, Color color);
	};
}