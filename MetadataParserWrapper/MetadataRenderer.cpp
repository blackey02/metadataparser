#include "stdafx.h"
#include "MetaDataRenderer.h"

using namespace MetaDataParser;

MetaDataRenderer::MetaDataRenderer(IMetaDataRenderer ^renderer) {
	_impl = new MetaDataRendererPrivate(renderer);
}

MetaDataRenderer::~MetaDataRenderer() {
	this->!MetaDataRenderer();
}

void MetaDataRenderer::!MetaDataRenderer() {
	delete _impl;
}

void MetaDataRenderer::SetMotionData(array<System::Byte> ^buffer, Color color) {
	pin_ptr<System::Byte> p = &buffer[buffer->GetLowerBound(0)];
	PelcoAPI::COLOR c;
	c.alpha = color.alpha;
	c.blue = color.blue;
	c.green = color.green;
	c.red = color.red;
	_impl->SetMotionData(p, (unsigned)buffer->Length, c);
}

void MetaDataRenderer::SetMotionData(MotionData ^data, Color color) {
	PelcoAPI::MotionData motionData;
	motionData.SetData(data->RawBuffer(), (unsigned)data->RawBufferLen());
	PelcoAPI::COLOR c;
	c.alpha = color.alpha;
	c.blue = color.blue;
	c.green = color.green;
	c.red = color.red;
	_impl->SetMotionData(motionData, c);
}

void MetaDataRenderer::SetDrawingData(array<System::Byte> ^buffer) {
	pin_ptr<System::Byte> p = &buffer[buffer->GetLowerBound(0)];
	_impl->SetDrawingData(p, buffer->Length);
}

void MetaDataRenderer::SetDrawingData(DrawingData ^data) {
	PelcoAPI::DrawingData drawingData;
	drawingData.SetData(data->RawBuffer(), (unsigned)data->RawBufferLen());
	_impl->SetDrawingData(drawingData);
}

void MetaDataRenderer::RenderMotionData() {
	_impl->RenderMotionData();
}

void MetaDataRenderer::RenderDrawingData(int width, int height) {
	_impl->RenderDrawingData(width, height);
}

void MetaDataRenderer::Reset() {
	_impl->Reset();
}