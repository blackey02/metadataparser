#include "stdafx.h"
#include "MetaDataRendererPrivate.h"
#include "CommonDefs.h"

using namespace MetaDataParser;

MetaDataRendererPrivate::MetaDataRendererPrivate(IMetaDataRenderer ^renderer) : PelcoAPI::MetaDataRenderer() {
	_renderer = renderer;
}

MetaDataRendererPrivate::~MetaDataRendererPrivate() {
}

//=========================================================================
// OPTIONAL base class overrides.
//=========================================================================
void MetaDataParser::MetaDataRendererPrivate::BeginDraw() throw() {
	_renderer->BeginDraw();
}

void MetaDataParser::MetaDataRendererPrivate::EndDraw() throw() {
	_renderer->EndDraw();
}

void MetaDataParser::MetaDataRendererPrivate::DrawLine(const PelcoAPI::VECTOR &v1, const PelcoAPI::VECTOR &v2, PelcoAPI::COLOR color) throw() {
	bool realLine = v1.x != v2.x || v1.y != v2.y;
	if(!realLine)
		return;
	
	Vector v3, v4;
	Color c1;
	v3.x = v1.x;
	v3.y = v1.y;
	v3.z = v1.z;
	v4.x = v2.x;
	v4.y = v2.y;
	v4.z = v2.z;
	c1.alpha = color.alpha;
	c1.blue = color.blue;
	c1.green = color.green;
	c1.red = color.red;
	_renderer->DrawLine(v3, v4, c1);
}

void MetaDataParser::MetaDataRendererPrivate::DrawRectangle(const PelcoAPI::VECTOR &v1, const PelcoAPI::VECTOR &v2, PelcoAPI::COLOR color, bool fill) throw()
{
	Vector v3, v4;
	Color c1;
	v3.x = v1.x;
	v3.y = v1.y;
	v3.z = v1.z;
	v4.x = v2.x;
	v4.y = v2.y;
	v4.z = v2.z;
	c1.alpha = color.alpha;
	c1.blue = color.blue;
	c1.green = color.green;
	c1.red = color.red;
	_renderer->DrawRectangle(v3, v4, c1, fill);
}

void MetaDataParser::MetaDataRendererPrivate::DrawPolygon(PelcoAPI::VECTOR *v1, unsigned int count, PelcoAPI::COLOR fillColor, PelcoAPI::COLOR borderColor, bool fill) throw()
{
	array<Vector> ^vectors = gcnew array<Vector>(count);
	pin_ptr<Vector> p = &vectors[vectors->GetLowerBound(0)];
	std::memcpy(p, v1, sizeof(Vector) * count);
	MetaDataParser::Color c1, c2;
	c1.alpha = fillColor.alpha;
	c1.blue = fillColor.blue;
	c1.green = fillColor.green;
	c1.red = fillColor.red;
	c2.alpha = borderColor.alpha;
	c2.blue = borderColor.blue;
	c2.green = borderColor.green;
	c2.red = borderColor.red;
	_renderer->DrawPolygon(vectors, c1, c2, fill);
}

void MetaDataParser::MetaDataRendererPrivate::DrawText(const std::string &text, const PelcoAPI::VECTOR &v1, PelcoAPI::COLOR color) throw()
{
	Vector v2;
	Color c1;
	System::String ^s = gcnew System::String(text.c_str());
	v2.x = v1.x;
	v2.y = v1.y;
	v2.z = v1.z;
	c1.alpha = color.alpha;
	c1.blue = color.blue;
	c1.green = color.green;
	c1.red = color.red;
	_renderer->DrawText(s, v2, c1);
}

PelcoAPI::VECTOR MetaDataParser::MetaDataRendererPrivate::TransformVectorForDisplay(const PelcoAPI::VECTOR &v) throw() {
	return v;
}
