#include "stdafx.h"
#include "DrawingData.h"

using namespace MetaDataParser;

DrawingData::DrawingData() {
	_impl = new PelcoAPI::DrawingData();
	_rawBuffer = NULL;
}

DrawingData::!DrawingData() {
	delete _impl;
	if(_rawBuffer)
		delete [] _rawBuffer;
}

DrawingData::~DrawingData() {
	this->!DrawingData();
}

bool DrawingData::SetData(unsigned char *buffer, unsigned int length) {
	if(_rawBuffer)
		delete [] _rawBuffer;

	_rawBufferLen = length;
	_rawBuffer = new unsigned char[_rawBufferLen];
	std::memcpy(_rawBuffer, buffer, _rawBufferLen);
	_impl->SetData(_rawBuffer, _rawBufferLen);
	return true;
}

void DrawingData::ResetPosition() {
	_impl->ResetPosition();
}

DrawingPrimitive^ DrawingData::GetNextPrimitive() {
	DrawingPrimitive ^primitive = nullptr;
	PelcoAPI::DrawingPrimitive *nextPrimitive = _impl->GetNextPrimitive();
	if(nextPrimitive)
		primitive = gcnew DrawingPrimitive(nextPrimitive);
	return primitive;
}

void DrawingData::Clear() {
	_impl->Clear();
}

unsigned char* DrawingData::RawBuffer() {
	return _rawBuffer;
}

int DrawingData::RawBufferLen() {
	return _rawBufferLen;
}