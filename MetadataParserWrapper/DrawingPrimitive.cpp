#include "stdafx.h"
#include "DrawingPrimitive.h"
#include "CommonDefs.h"

using namespace MetaDataParser;

DrawingPrimitive::DrawingPrimitive(PelcoAPI::DrawingPrimitive *primitive) {
	_impl = primitive;
}

DrawingPrimitive::!DrawingPrimitive() {
	delete _impl;
}

DrawingPrimitive::~DrawingPrimitive() {
	this->!DrawingPrimitive();
}

PrimitiveType DrawingPrimitive::GetPrimitiveType() {
	return (PrimitiveType)_impl->GetPrimitiveType();;
}

unsigned int DrawingPrimitive::GetNumberOfPoints() {
	return _impl->GetNumberOfPoints();
}

Point DrawingPrimitive::GetPoint(unsigned int index) {
	PelcoAPI::POINT uPoint;
	_impl->GetPoint(index, uPoint);
	Point point;
	point.x = uPoint.x;
	point.y = uPoint.y;
	return point;
}

PrimitiveColor DrawingPrimitive::GetDrawColor() {
	return (PrimitiveColor)_impl->GetDrawColor();
}

System::String^ DrawingPrimitive::GetText() {
	std::string text;
	_impl->GetText(text);
	return gcnew System::String(text.c_str());
}

PrimitiveColor DrawingPrimitive::GetFillColor() {
	return (PrimitiveColor)_impl->GetFillColor();
}

unsigned char DrawingPrimitive::GetObjectId() {
	return _impl->GetObjectId();
}

ObjectType DrawingPrimitive::GetObjectType() {
	return (ObjectType)_impl->GetObjectType();
}

Direction DrawingPrimitive::GetDirection() {
	return (Direction)_impl->GetDirection();
}

bool DrawingPrimitive::GetArrowHead(Point %point1, Point %point2, Point %point3) {
	PelcoAPI::POINT one, two, three;
	_impl->GetArrowHead(one, two, three);
	point1.x = one.x;
	point1.y = one.y;
	point2.x = two.x;
	point2.y = two.y;
	point3.x = three.x;
	point3.y = three.y;
	return true;
}

bool DrawingPrimitive::GetArrowTail(Point %point1, Point %point2) {
	PelcoAPI::POINT one, two;
	_impl->GetArrowTail(one, two);
	point1.x = one.x;
	point1.y = one.y;
	point2.x = two.x;
	point2.y = two.y;
	return true;
}